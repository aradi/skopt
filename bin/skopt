#!/usr/bin/env python3
import os
import argparse
from skopt.core.skopt import SKOPT


def main():

    # argument parsing at start
    # -------------------------------------------------------------------
    parser = argparse.ArgumentParser(
            description="Tool for optimising Slater-Koster tables for DFTB."
            )
    parser.add_argument(
            "skopt_input", type=str, default="skopt_in.yaml", action="store", 
            help="YAML input file: objectives, tasks, executables, optimisation options."
            )
    parser.add_argument(
            '-v', '--verbose', dest='verbose', default=False, action='store_true',
            help="Verbose console output (include  full log as in ./skopt.debug.log)"
            )
    parser.add_argument(
            '-n', '--dry_run', dest='dry_run', default=False, action='store_true',
            help="Do not run; Only report the setup (tasklist, objectives, optimisation)."
            )
    parser.add_argument(
            '-e', '--evaluate_only', dest='evaluate_only', default=False, action='store_true',
            help="Do not optimise, but execute the task list and evaluate fitness."
            )
    args = parser.parse_args()

    skopt = SKOPT(infile=args.skopt_input, verbose=args.verbose)

    if not args.dry_run:
        skopt(evalonly=args.evaluate_only)
    else:
        skopt.logger.warning('DRY RUN: reporting setup only!')
        skopt.logger.info(skopt.evaluator)


if __name__ == "__main__":
    main()
