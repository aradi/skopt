���u      �docutils.nodes��document���)}���}�(�
decoration�N�settings��docutils.frontend��Values���)}���}�(�toc_backlinks��entry��pep_file_url_template��pep-%04d��	datestamp�N�sectnum_xform�K�config�N�doctitle_xform���_source��3C:\Users\admin\Software\skopt\docs\source\about.rst��rfc_base_url��https://tools.ietf.org/html/��	traceback���strict_visitor�N�sectsubtitle_xform���output_encoding_error_handler��strict��_disable_config�N�footnote_backlinks�K�_destination�N�env�N�pep_base_url�� https://www.python.org/dev/peps/��
source_url�N�dump_settings�N�output_encoding��utf-8��rfc_references�N�report_level�K�smart_quotes���dump_internals�N�	tab_width�K�file_insertion_enabled���source_link�N�language_code��en��expose_internals�N�auto_id_prefix��id��input_encoding��	utf-8-sig��strip_comments�N�
halt_level�K�input_encoding_error_handler�h�title�N�error_encoding��cp65001��dump_pseudo_xml�N�_config_files�]��trim_footnote_reference_space���	id_prefix�� ��strip_elements_with_classes�N�debug�N�cloak_email_addresses���dump_transforms�N�raw_enabled�K�embed_stylesheet���error_encoding_error_handler��backslashreplace��	generator�N�record_dependencies�N�character_level_inline_markup���warning_stream�N�strip_classes�N�exit_status_level�K�syntax_highlight��long��docinfo_xform�K�pep_references�N�gettext_compact��ub�reporter�N�ids�}�(�index-0�h �section���)}���}�(�expect_referenced_by_id�}�(hYh �target���)}���}�(�children�]��
attributes�}�(�ids�]��backrefs�]��classes�]��refid�hY�dupnames�]��names�]�u�tagname�hahh�	rawsource�hB�parent�h�line�K�source�hub�about�hb)}���}�(h_}�hYhdshf]�hh}�(hj]�hl]�hn]�hphzhq]�hs]�uhuhahhhv�
.. _about:�hwhhxKhyh�expect_referenced_by_name�}�ubuhf]�(h h:��)}���}�(hf]�h �Text����About���}���}�(hv�About�hwh�ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh:hhhvh�hwh]hxKhyhubh �	paragraph���)}���}�(hf]�h�X  SKOPT is a software tool intended to automate the optimisation of
parameters for the Density Functional Tight Binding (DFTB) theory.
It allows a flexible and simultaneous use of diverse reference data,
e.g. from DFT calculations or experimentally obtained physical quantities.���}���}�(hvX  SKOPT is a software tool intended to automate the optimisation of
parameters for the Density Functional Tight Binding (DFTB) theory.
It allows a flexible and simultaneous use of diverse reference data,
e.g. from DFT calculations or experimentally obtained physical quantities.�hwh�ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhvh�hwh]hxK	hyhubhb)}���}�(hf]�hh}�(hj]�hl]�hn]�hp�fig-1�hq]�hs]�uhuhahhhv�.. _`Fig. 1`:�hwh]hxKhyhubh �figure���)}���}�(h_}�h�h�shf]�(h �image���)}���}�(hf]�hh}�(�uri��../static/skopt.diagram.png�hj]�hl]�hn]��
candidates�}��*�h�shq]�hs]��width��80%�uhuh�hv�t.. figure:: ../static/skopt.diagram.png
        :width: 80%

        **Fig. 1. Conceptual block diagram of SKOPT.**
�hwh�hxKhyhubh �caption���)}���}�(hf]�h �strong���)}���}�(hf]�h��*Fig. 1. Conceptual block diagram of SKOPT.���}���}�(hvhBhwh�ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�.**Fig. 1. Conceptual block diagram of SKOPT.**�hwh�ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvh�hwh�hxKhyhubehh}�(hn]�hq]�hj]�(�id2�h�ehl]�hs]��fig. 1�auhuh�hhhvhBhwh]hxKhyhh�}�h�h�subh[)}���}�(hf]�(h�)}���}�(hf]�h��Conceptual Overview���}���}�(hv�Conceptual Overview�hwj  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh:hhhvj
  hwh�hxKhyhubh�)}���}�(hf]�h��sThe conceptual diagram of SKOPT is shown in Fig. 1, where the relation between
the following entities is suggested:���}���}�(hv�sThe conceptual diagram of SKOPT is shown in Fig. 1, where the relation between
the following entities is suggested:�hwj  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhvj  hwh�hxKhyhubh �block_quote���)}���}�(hf]�h �bullet_list���)}���}�(hf]�(h �	list_item���)}���}�(hf]�h�)}���}�(hf]�(h�)}���}�(hf]�h��Model���}���}�(hvhBhwj8  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�	**Model**�hwj4  ubh�XF   -- a collection of executables outside SKOPT
that produce some data; In the context of DFTB parameterisation
the model may encompass Slate-Koster table generation (driven by some
parameters per chemical element), and a number of DFTB calculations
that yield total energy and band-structure for one or more atomic
structures. ���}���}�(hvXF   -- a collection of executables outside SKOPT
that produce some data; In the context of DFTB parameterisation
the model may encompass Slate-Koster table generation (driven by some
parameters per chemical element), and a number of DFTB calculations
that yield total energy and band-structure for one or more atomic
structures. �hwj4  ubh �emphasis���)}���}�(hf]�h���SKOPT features a dynamic model setup via the declaration
of a 'Model Task-List' in the SKOPT input file; There is no hard-coded
application-specific model.���}���}�(hvhBhwjP  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv��*SKOPT features a dynamic model setup via the declaration
of a 'Model Task-List' in the SKOPT input file; There is no hard-coded
application-specific model.*�hwj4  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvX�  **Model** -- a collection of executables outside SKOPT
that produce some data; In the context of DFTB parameterisation
the model may encompass Slate-Koster table generation (driven by some
parameters per chemical element), and a number of DFTB calculations
that yield total energy and band-structure for one or more atomic
structures. *SKOPT features a dynamic model setup via the declaration
of a 'Model Task-List' in the SKOPT input file; There is no hard-coded
application-specific model.*�hwj0  hxKhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hvX�  **Model** -- a collection of executables outside SKOPT
that produce some data; In the context of DFTB parameterisation
the model may encompass Slate-Koster table generation (driven by some
parameters per chemical element), and a number of DFTB calculations
that yield total energy and band-structure for one or more atomic
structures. *SKOPT features a dynamic model setup via the declaration
of a 'Model Task-List' in the SKOPT input file; There is no hard-coded
application-specific model.*
�hwj*  ubj.  )}���}�(hf]�h�)}���}�(hf]�(h�)}���}�(hf]�h��
Objectives���}���}�(hvhBhwjv  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�**Objectives**�hwjr  ubh�X    -- a set of single valued functions that depend on the
model parameters; Typical example is a root-mean-squared deviation
between some reference data (e.g. band-structure calculated by DFT)
and the model data (e.g. the band-structure calculated by DFTB).
���}���}�(hvX    -- a set of single valued functions that depend on the
model parameters; Typical example is a root-mean-squared deviation
between some reference data (e.g. band-structure calculated by DFT)
and the model data (e.g. the band-structure calculated by DFTB).
�hwjr  ubjN  )}���}�(hf]�h���SKOPT provides a generic facility for declaring objective function
by specifying a list of Objectives in the input file; the specification
includes instruction on accessing reference data and determines a
query into the model and reference databases.���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv��*SKOPT provides a generic facility for declaring objective function
by specifying a list of Objectives in the input file; the specification
includes instruction on accessing reference data and determines a
query into the model and reference databases.*�hwjr  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvX
  **Objectives** -- a set of single valued functions that depend on the
model parameters; Typical example is a root-mean-squared deviation
between some reference data (e.g. band-structure calculated by DFT)
and the model data (e.g. the band-structure calculated by DFTB).
*SKOPT provides a generic facility for declaring objective function
by specifying a list of Objectives in the input file; the specification
includes instruction on accessing reference data and determines a
query into the model and reference databases.*�hwjn  hxK$hyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hvX  **Objectives** -- a set of single valued functions that depend on the
model parameters; Typical example is a root-mean-squared deviation
between some reference data (e.g. band-structure calculated by DFT)
and the model data (e.g. the band-structure calculated by DFTB).
*SKOPT provides a generic facility for declaring objective function
by specifying a list of Objectives in the input file; the specification
includes instruction on accessing reference data and determines a
query into the model and reference databases.*
�hwj*  ubj.  )}���}�(hf]�h�)}���}�(hf]�(h�)}���}�(hf]�h��Reference data���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�**Reference data**�hwj�  ubh��� -- a set of data items that we want the model to be
able to reproduce within certain error tolerance; Reference data may
come from DFT calculations or be experimentally obtained.
���}���}�(hv�� -- a set of data items that we want the model to be
able to reproduce within certain error tolerance; Reference data may
come from DFT calculations or be experimentally obtained.
�hwj�  ubjN  )}���}�(hf]�h���SKOPT admits explicit reference data in the input file, or instructions
on how to obtain reference data by accessing and interpreting
external files; support for database query is under development too.���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv��*SKOPT admits explicit reference data in the input file, or instructions
on how to obtain reference data by accessing and interpreting
external files; support for database query is under development too.*�hwj�  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvX�  **Reference data** -- a set of data items that we want the model to be
able to reproduce within certain error tolerance; Reference data may
come from DFT calculations or be experimentally obtained.
*SKOPT admits explicit reference data in the input file, or instructions
on how to obtain reference data by accessing and interpreting
external files; support for database query is under development too.*�hwj�  hxK-hyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hvX�  **Reference data** -- a set of data items that we want the model to be
able to reproduce within certain error tolerance; Reference data may
come from DFT calculations or be experimentally obtained.
*SKOPT admits explicit reference data in the input file, or instructions
on how to obtain reference data by accessing and interpreting
external files; support for database query is under development too.*
�hwj*  ubj.  )}���}�(hf]�h�)}���}�(hf]�(h�)}���}�(hf]�h��Cost function���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�**Cost function**�hwj�  ubh��� -- a scalar function of the individual objectives
mentioned above that yields a single number representative of the
quality of a given set of parameter values. ���}���}�(hv�� -- a scalar function of the individual objectives
mentioned above that yields a single number representative of the
quality of a given set of parameter values. �hwj�  ubjN  )}���}�(hf]�h��_Currently SKOPT supports
only weighted root mean squared deviation of the objectives from zero.���}���}�(hvhBhwj  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv�a*Currently SKOPT supports
only weighted root mean squared deviation of the objectives from zero.*�hwj�  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvX  **Cost function** -- a scalar function of the individual objectives
mentioned above that yields a single number representative of the
quality of a given set of parameter values. *Currently SKOPT supports
only weighted root mean squared deviation of the objectives from zero.*�hwj�  hxK4hyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hvX  **Cost function** -- a scalar function of the individual objectives
mentioned above that yields a single number representative of the
quality of a given set of parameter values. *Currently SKOPT supports
only weighted root mean squared deviation of the objectives from zero.*
�hwj*  ubj.  )}���}�(hf]�h�)}���}�(hf]�(h�)}���}�(hf]�h��	Optimiser���}���}�(hvhBhwj*  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�**Optimiser**�hwj&  ubh��p -- an algorithm for efficient exploration of the parameter
space with the aim of minimising the cost function. ���}���}�(hv�p -- an algorithm for efficient exploration of the parameter
space with the aim of minimising the cost function. �hwj&  ubjN  )}���}�(hf]�h��;SKOPT
features particle-swarm-optimisation (PSO) algorithm.���}���}�(hvhBhwj@  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv�=*SKOPT
features particle-swarm-optimisation (PSO) algorithm.*�hwj&  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv��**Optimiser** -- an algorithm for efficient exploration of the parameter
space with the aim of minimising the cost function. *SKOPT
features particle-swarm-optimisation (PSO) algorithm.*�hwj"  hxK9hyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hv��**Optimiser** -- an algorithm for efficient exploration of the parameter
space with the aim of minimising the cost function. *SKOPT
features particle-swarm-optimisation (PSO) algorithm.*
�hwj*  ubehh}�(�bullet�h�hj]�hl]�hn]�hq]�hs]�uhuj'  hvhBhwj$  hxKhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj!  hhhvhBhwh�hxNhyNubh�)}���}�(hf]�(h��The sole purpose of the ���}���}�(hv�The sole purpose of the �hwjk  ubjN  )}���}�(hf]�h��	Optimiser���}���}�(hvhBhwju  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv�*Optimiser*�hwjk  ubh��v in Fig. 1 is to generate parameters in a way
that does not depend on the specifics of the model being optimised.
The ���}���}�(hv�v in Fig. 1 is to generate parameters in a way
that does not depend on the specifics of the model being optimised.
The �hwjk  ubjN  )}���}�(hf]�h��	Evaluator���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv�*Evaluator*�hwjk  ubh��> in Fig. 1 acts as an interface between the embodiment of the
���}���}�(hv�> in Fig. 1 acts as an interface between the embodiment of the
�hwjk  ubjN  )}���}�(hf]�h��Model���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv�*Model*�hwjk  ubh��. by one or more external executables, and the ���}���}�(hv�. by one or more external executables, and the �hwjk  ubjN  )}���}�(hf]�h��	Optimiser���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhujM  hv�*Optimiser*�hwjk  ubh��.���}���}�(hv�.�hwjk  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhvX#  The sole purpose of the *Optimiser* in Fig. 1 is to generate parameters in a way
that does not depend on the specifics of the model being optimised.
The *Evaluator* in Fig. 1 acts as an interface between the embodiment of the
*Model* by one or more external executables, and the *Optimiser*.�hwh�hxK=hyhubh�)}���}�(hf]�(h���The declaration of objectives and model tasks, as well as the overall functionality
of SKOPT is controlled by an input file (in ���}���}�(hv��The declaration of objectives and model tasks, as well as the overall functionality
of SKOPT is controlled by an input file (in �hwj�  ubh �	reference���)}���}�(hf]�h��YAML���}���}�(hvhBhwj�  ubahh}�(�name��YAML�hj]�hl]�hn]�hq]�hs]��refuri��*http://pyyaml.org/wiki/PyYAMLDocumentation�uhuj�  hv�YAML_�hwj�  �resolved�Kubh��2 format), where the user must
define as a minimum:���}���}�(hv�2 format), where the user must
define as a minimum:�hwj�  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhv��The declaration of objectives and model tasks, as well as the overall functionality
of SKOPT is controlled by an input file (in YAML_ format), where the user must
define as a minimum:�hwh�hxKBhyhubj"  )}���}�(hf]�h �enumerated_list���)}���}�(hf]�(j.  )}���}�(hf]�h�)}���}�(hf]�h��DA list of tasks that must be executed in order to obtain model data.���}���}�(hv�DA list of tasks that must be executed in order to obtain model data.�hwj  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvj  hwj  hxKFhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hv�EA list of tasks that must be executed in order to obtain model data.
�hwj  ubj.  )}���}�(hf]�h�)}���}�(hf]�h��LA list of objectives that must be evaluated in order to assess overall cost.���}���}�(hv�LA list of objectives that must be evaluated in order to assess overall cost.�hwj+  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvj3  hwj'  hxKHhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hv�MA list of objectives that must be evaluated in order to assess overall cost.
�hwj  ubj.  )}���}�(hf]�h�)}���}�(hf]�h��8The optimisation strategy -- algorithm, parameters, etc.���}���}�(hv�8The optimisation strategy -- algorithm, parameters, etc.�hwjF  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvjN  hwjB  hxKJhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hv�9The optimisation strategy -- algorithm, parameters, etc.
�hwj  ubj.  )}���}�(hf]�h�)}���}�(hf]�h��:Aliases to complex commands involving external executables���}���}�(hv�:Aliases to complex commands involving external executables�hwja  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvji  hwj]  hxKLhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hv�;Aliases to complex commands involving external executables
�hwj  ubehh}�(�suffix�j�  �prefix�hBhj]�hl]��enumtype��arabic�hn]�hq]�hs]�uhuj  hvhBhwj  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj!  hhhvhBhwh�hxNhyNubh�)}���}�(hf]�h��;The optimisation loop realised by SKOPT is shown in Fig. 2.���}���}�(hv�;The optimisation loop realised by SKOPT is shown in Fig. 2.�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhvj�  hwh�hxKNhyhubhb)}���}�(hf]�hh}�(hj]�hl]�hn]�hp�fig-2�hq]�hs]�uhuhahhhv�.. _`Fig. 2`:�hwh�hxKPhyhubh�)}���}�(h_}�j�  j�  shf]�(h�)}���}�(hf]�hh}�(�uri��$../static/optimisation.flowchart.png�hj]�hl]�hn]�h�}�h�j�  shq]�hs]��width��70%�uhuh�hv��.. figure:: ../static/optimisation.flowchart.png
        :width: 70%

        **Fig. 2. Optimisation loop realised by SKOPT.**


�hwj�  hxKUhyhubh�)}���}�(hf]�h�)}���}�(hf]�h��,Fig. 2. Optimisation loop realised by SKOPT.���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�0**Fig. 2. Optimisation loop realised by SKOPT.**�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvj�  hwj�  hxKUhyhubehh}�(hn]�hq]�hj]�(�id3�j�  ehl]�hs]��fig. 2�auhuh�hhhvhBhwh�hxKUhyhh�}�j�  j�  subehh}�(hn]�hq]�hj]��conceptual-overview�ahl]�hs]��conceptual overview�auhuhZhhhvhBhwh]hxKhyhubh[)}���}�(hf]�(h�)}���}�(hf]�h��Implementation Overview���}���}�(hv�Implementation Overview�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh:hhhvj�  hwj�  hxKZhyhubh�)}���}�(hf]�(h��SKOPT is implemented in ���}���}�(hv�SKOPT is implemented in �hwj�  ubj�  )}���}�(hf]�h��Python���}���}�(hvhBhwj  ubahh}�(�name��Python�hj]�hl]�hn]�hq]�hs]�j�  �http://www.python.org�uhuj�  hv�	`Python`_�hwj�  j�  Kubh��L and currently uses a Particle Swarm
Optimisation (PSO) engine based on the ���}���}�(hv�L and currently uses a Particle Swarm
Optimisation (PSO) engine based on the �hwj�  ubj�  )}���}�(hf]�h��DEAP���}���}�(hvhBhwj  ubahh}�(�name��DEAP�hj]�hl]�hn]�hq]�hs]�j�  �%http://deap.readthedocs.io/en/master/�uhuj�  hv�`DEAP`_�hwj�  j�  Kubh��W library for evolutionary
algorithms. Its control is done via an input file written in ���}���}�(hv�W library for evolutionary
algorithms. Its control is done via an input file written in �hwj�  ubj�  )}���}�(hf]�h��YAML���}���}�(hvhBhwj4  ubahh}�(�name��YAML�hj]�hl]�hn]�hq]�hs]�j�  j�  uhuj�  hv�YAML_�hwj�  j�  Kubh��.���}���}�(hvj�  hwj�  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhv��SKOPT is implemented in `Python`_ and currently uses a Particle Swarm
Optimisation (PSO) engine based on the `DEAP`_ library for evolutionary
algorithms. Its control is done via an input file written in YAML_.�hwj�  hxK\hyhubh�)}���}�(hf]�(h��+Currently SKOPT provides two sub-packages: ���}���}�(hv�+Currently SKOPT provides two sub-packages: �hwjR  ubh �literal���)}���}�(hf]�h��core���}���}�(hvhBhwj^  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj[  hv�``core``�hwjR  ubh�� and ���}���}�(hv� and �hwjR  ubj\  )}���}�(hf]�h��	dftbutils���}���}�(hvhBhwjt  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj[  hv�``dftbutils``�hwjR  ubh��.���}���}�(hvj�  hwjR  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhv�FCurrently SKOPT provides two sub-packages: ``core`` and ``dftbutils``.�hwj�  hxK`hyhubh�)}���}�(hf]�(h��The ���}���}�(hv�The �hwj�  ubj\  )}���}�(hf]�h��core���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj[  hv�``core``�hwj�  ubh��3 package is of general nature, and its coupling to
���}���}�(hv�3 package is of general nature, and its coupling to
�hwj�  ubj\  )}���}�(hf]�h��	dftbutils���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj[  hv�``dftbutils``�hwj�  ubh��h is only via a tasks dictionary, through which SKOPT
learns how to acquire data related to a DFTB model.���}���}�(hv�h is only via a tasks dictionary, through which SKOPT
learns how to acquire data related to a DFTB model.�hwj�  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhv��The ``core`` package is of general nature, and its coupling to
``dftbutils`` is only via a tasks dictionary, through which SKOPT
learns how to acquire data related to a DFTB model.�hwj�  hxKbhyhubh�)}���}�(hf]�(h��The ���}���}�(hv�The �hwj�  ubj\  )}���}�(hf]�h��	dftbutils���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj[  hv�``dftbutils``�hwj�  ubh��� package concerns with all that is necessary to obtain
data from a DFTB calculation. Presently, this package is limited in its
support to the executables provided by BCCMS at the University of Bremen,
Germany.
This assumes:���}���}�(hv�� package concerns with all that is necessary to obtain
data from a DFTB calculation. Presently, this package is limited in its
support to the executables provided by BCCMS at the University of Bremen,
Germany.
This assumes:�hwj�  ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhv��The ``dftbutils`` package concerns with all that is necessary to obtain
data from a DFTB calculation. Presently, this package is limited in its
support to the executables provided by BCCMS at the University of Bremen,
Germany.
This assumes:�hwj�  hxKfhyhubj"  )}���}�(hf]�j(  )}���}�(hf]�(j.  )}���}�(hf]�h�)}���}�(hf]�(h��;SKGEN is used for Slater-Koster File (.skf) generation (by ���}���}�(hv�;SKGEN is used for Slater-Koster File (.skf) generation (by �hwj   ubj\  )}���}�(hf]�h��
slateratom���}���}�(hvhBhwj
  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj[  hv�``slateratom``�hwj   ubh��,
���}���}�(hv�,
�hwj   ubj\  )}���}�(hf]�h��twocnt���}���}�(hvhBhwj   ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj[  hv�
``twocnt``�hwj   ubh��, and SKGEN),���}���}�(hv�, and SKGEN),�hwj   ubehh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�bSKGEN is used for Slater-Koster File (.skf) generation (by ``slateratom``,
``twocnt``, and SKGEN),�hwj�  hxKlhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hv�bSKGEN is used for Slater-Koster File (.skf) generation (by ``slateratom``,
``twocnt``, and SKGEN),�hwj�  ubj.  )}���}�(hf]�h�)}���}�(hf]�h��)DFTB+ is used as the DFTB calculator, and���}���}�(hv�)DFTB+ is used as the DFTB calculator, and�hwjH  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvjP  hwjD  hxKnhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hvjP  hwj�  ubj.  )}���}�(hf]�h�)}���}�(hf]�h��Udp_bands is used as post-processor of eigenvalue data to produce
band-structure data.���}���}�(hv�Udp_bands is used as post-processor of eigenvalue data to produce
band-structure data.�hwjb  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvjj  hwj^  hxKohyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj-  hv�Vdp_bands is used as post-processor of eigenvalue data to produce
band-structure data.
�hwj�  ubehh}�(j^  h�hj]�hl]�hn]�hq]�hs]�uhuj'  hvhBhwj�  hxKlhyhubahh}�(hn]�hq]�hj]�hl]�hs]�uhuj!  hhhvhBhwj�  hxNhyNubh�)}���}�(hf]�h���However, an easy extension to alternative tool-flow is possible, and current
development aims to completely decouple model execution from the core of SKOPT.���}���}�(hv��However, an easy extension to alternative tool-flow is possible, and current
development aims to completely decouple model execution from the core of SKOPT.�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhvj�  hwj�  hxKrhyhub�sphinx.addnodes��seealso���)}���}�(hf]�(h�)}���}�(hf]�h��Subpackages and modules���}���}�(hv�Subpackages and modules�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvj�  hwj�  hxKvhyhubh�)}���}�(hf]�h��Development���}���}�(hv�Development�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvj�  hwj�  hxKxhyhubehh}�(hn]�hq]�hj]�hl]�hs]�uhuj�  hhhv�$Subpackages and modules

Development�hwj�  hxNhyhubehh}�(hn]�hq]�hj]��implementation-overview�ahl]�hs]��implementation overview�auhuhZhhhvhBhwh]hxKZhyhubh[)}���}�(hf]�(h�)}���}�(hf]�h��
Extensions���}���}�(hv�
Extensions�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh:hhhvj�  hwj�  hxK{hyhubh�)}���}�(hf]�h�X�  The design of SKOPT features weak coupling between the core engine that
deals with a general multi-objective optimisation problem, and the specifics
of model execution that yields model data for a given set of parameter values.
Therefore, its extension beyond DFTB parameterisation -- e.g. to the closely
related problems of parameter optimisation for empirical tight-bining (ETB)
Hamiltonians or classical interatomic potentials for molecular dynamics,
should be straightforward.���}���}�(hvX�  The design of SKOPT features weak coupling between the core engine that
deals with a general multi-objective optimisation problem, and the specifics
of model execution that yields model data for a given set of parameter values.
Therefore, its extension beyond DFTB parameterisation -- e.g. to the closely
related problems of parameter optimisation for empirical tight-bining (ETB)
Hamiltonians or classical interatomic potentials for molecular dynamics,
should be straightforward.�hwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hhhvj�  hwj�  hxK|hyhubhb)}���}�(hf]��
referenced�Khh}�(hj]��python�ahl]�hn]�hq]�hs]��python�aj�  j  uhuhahhhv�#.. _`Python`: http://www.python.org�hwj�  hxK�hyhubhb)}���}�(hf]�hh}�(hj]��dftb�ahl]�hn]�hq]�hs]��dftb+�aj�  �http://www.dftb-plus.info/�uhuhahhhv�'.. _`DFTB+`: http://www.dftb-plus.info/�hwj�  hxK�hyhubhb)}���}�(hf]�hh}�(hj]��lodestar�ahl]�hn]�hq]�hs]��lodestar�aj�  �&http://yangtze.hku.hk/new/software.php�uhuhahhhv�4.. _Lodestar: http://yangtze.hku.hk/new/software.php�hwj�  hxK�hyhubhb)}���}�(hf]�hh}�(hj]��dftb-org�ahl]�hn]�hq]�hs]��dftb.org�aj�  �http://www.dftb.org/home/�uhuhahhhv�'.. _dftb.org: http://www.dftb.org/home/�hwj�  hxK�hyhubhb)}���}�(hf]�hh}�(hj]��mit-license�ahl]�hn]�hq]�hs]��mit license�aj�  �#https://opensource.org/licenses/MIT�uhuhahhhv�6.. _`MIT license`: https://opensource.org/licenses/MIT�hwj�  hxK�hyhubhb)}���}�(hf]�j�  Khh}�(hj]��deap�ahl]�hn]�hq]�hs]��deap�aj�  j+  uhuhahhhv�1.. _`DEAP`: http://deap.readthedocs.io/en/master/�hwj�  hxK�hyhubhb)}���}�(hf]�j�  Khh}�(hj]��yaml�ahl]�hn]�hq]�hs]��yaml�aj�  j�  uhuhahhhv�6.. _`YAML`: http://pyyaml.org/wiki/PyYAMLDocumentation�hwj�  hxK�hyhubehh}�(hn]�hq]�hj]��
extensions�ahl]�hs]��
extensions�auhuhZhhhvhBhwh]hxK{hyhubej�  Khh}�(hn]�hq]��about�ahj]�(hzhY�id1�ehl]�hs]��about�auhuhZhhhvhBhwhhxKhyhh�}�j^  h|subj�  j�  j[  h]j�  j�  j  j  jG  jB  j  j  j:  j5  h�h�h�h�j�  j�  jR  j�  hzh]j,  j'  j�  j�  j  j�  j�  h�u�substitution_names�}��indirect_targets�]��symbol_footnotes�]��refnames�}�(�deap�]�j  a�yaml�]�(j�  j4  e�python�]�j  au�citation_refs�}��refids�}�(j�  ]�j�  ahY]�hdahz]�h|ah�]�h�au�	citations�]��autofootnotes�]�hh}�(�source�hhj]�hl]�hn]�hq]�hs]�uhuh�substitution_defs�}��	footnotes�]��symbol_footnote_start�K �id_start�Khf]�(j�  �index���)}���}�(hf]�hh}�(hj]�hl]�hn]��entries�]�(�single��about�hYhBNt�ahq]�hs]��inline��uhuj�  hhhvhBhwhhxKhyhubhdh|h]e�symbol_footnote_refs�]��autofootnote_start�KhvhB�nameids�}�(j�  j�  h�h�j#  j  j  j  jL  jG  j  j  j1  j,  j�  j�  j?  j:  j�  j�  j�  j�  jU  jR  j^  hzu�parse_messages�]�h �system_message���)}���}�(hf]�h�)}���}�(hf]�h��(Duplicate implicit target name: "about".���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hv�(Duplicate implicit target name: "about".�hwj�  ubahh}�(�source�hhj]�hl]�j[  ahn]��line�K�level�Khq]�hs]��type��INFO�uhuj�  hhhvhBhwh]hxKhyhuba�current_line�N�current_source�Nhh�transform_messages�]�(j�  )}���}�(hf]�h�)}���}�(hf]�h��-Hyperlink target "index-0" is not referenced.���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwj�  ubahh}�(�source�hhj]�hl]�hn]��line�K�level�Khq]�hs]��type�j�  uhuj�  hvhBubj�  )}���}�(hf]�h�)}���}�(hf]�h��+Hyperlink target "about" is not referenced.���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwj�  ubahh}�(�source�hhj]�hl]�hn]��line�K�level�Khq]�hs]��type�j�  uhuj�  hvhBubj�  )}���}�(hf]�h�)}���}�(hf]�h��+Hyperlink target "fig-1" is not referenced.���}���}�(hvhBhwj  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwj  ubahh}�(�source�hhj]�hl]�hn]��line�K�level�Khq]�hs]��type�j�  uhuj�  hvhBubj�  )}���}�(hf]�h�)}���}�(hf]�h��+Hyperlink target "fig-2" is not referenced.���}���}�(hvhBhwj"  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwj  ubahh}�(�source�hhj]�hl]�hn]��line�KP�level�Khq]�hs]��type�j�  uhuj�  hvhBubj�  )}���}�(hf]�h�)}���}�(hf]�h��+Hyperlink target "dftb+" is not referenced.���}���}�(hvhBhwj?  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwj;  ubahh}�(�source�hhj]�hl]�hn]��line�K��level�Khq]�hs]��type�j�  uhuj�  hvhBubj�  )}���}�(hf]�h�)}���}�(hf]�h��.Hyperlink target "lodestar" is not referenced.���}���}�(hvhBhwj\  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwjX  ubahh}�(�source�hhj]�hl]�hn]��line�K��level�Khq]�hs]��type�j�  uhuj�  hvhBubj�  )}���}�(hf]�h�)}���}�(hf]�h��.Hyperlink target "dftb.org" is not referenced.���}���}�(hvhBhwjy  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwju  ubahh}�(�source�hhj]�hl]�hn]��line�K��level�Khq]�hs]��type�j�  uhuj�  hvhBubj�  )}���}�(hf]�h�)}���}�(hf]�h��1Hyperlink target "mit license" is not referenced.���}���}�(hvhBhwj�  ubahh}�(hn]�hq]�hj]�hl]�hs]�uhuh�hvhBhwj�  ubahh}�(�source�hhj]�hl]�hn]��line�K��level�Khq]�hs]��type�j�  uhuj�  hvhBube�	nametypes�}�(j�  Nh��j#  �j  �jL  �j  �j1  �j�  �j?  �j�  Nj�  �jU  Nj^  �u�transformer�N�footnote_refs�}��autofootnote_refs�]�ub.