.. index:: license

.. _license:

=======
License
=======

SKOPT is distributed under `The MIT License`_.

.. _The MIT license: https://opensource.org/licenses/MIT
