SKOPT is a software tool intended to automate the optimisation of 
parameters for the Density Functional Tight Binding (DFTB) theory
as implemented in dftb+ (BCCMS Bremen), lodestar (The University 
of Hong Kong) and other computer codes.

Documentation: http://skopt.readthedocs.io

SKOPT is under MIT licence.
